class Department
  include Mongoid::Document
  field :name, type: String

  has_many :employees, autosave: true

  validates :name, length: { minimum: 2, :message => "What" }, format: { with: /\A[a-zA-Z ]+\z/, message: "only allows letters" }, :uniqueness => true
end
