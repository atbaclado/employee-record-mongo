class User
  include Mongoid::Document
  include ActiveModel::SecurePassword

  field :username, type: String
  field :password_digest, type: String
  field :employee_id, type: BSON::ObjectId
  has_secure_password

  validates :password, presence: true, length: { minimum: 6}
  validates :password_confirmation, presence: true
  validates :username, :employee_id, :presence => true, :uniqueness => true
end
