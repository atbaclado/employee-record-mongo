class JobAssignment
  include Mongoid::Document
  field :start_date, type: Date
  field :end_date, type: Date

  belongs_to :job_title
  belongs_to :employee
end
