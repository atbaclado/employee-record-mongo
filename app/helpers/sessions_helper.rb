module SessionsHelper
	# Logs in the given user.
	def log_in(user)
	  session[:user_id] = user.id
	end

	# Returns the current logged-in user (if any).
	def current_user
		if @current_user.nil? && !session[:user_id].nil?
		  @current_user = User.find(session[:user_id])
		else
		  @current_user
		end
	end

	# Check if user is Admin as Job Title
	def admin?
		admin = JobTitle.first
		job_assign = JobAssignment.find_by(job_title_id: admin.id, employee_id: @current_user.employee_id)
	end

	# Returns true if the user is logged in, false otherwise.
	def logged_in?
	    !current_user.nil?
	end

	# Logs out the current user.
	def log_out
	    session.delete(:user_id)
	    @current_user = nil
	end
end
