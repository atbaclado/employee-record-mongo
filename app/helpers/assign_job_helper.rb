module AssignJobHelper
	def display_name(employee)
		"#{employee.first_name} #{employee.middle_name} #{employee.last_name}"
	end
end
