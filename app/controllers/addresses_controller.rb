class AddressesController < ApplicationController
	before_action :set_employee, except: [:show, :new]

  def index
  end

  def new
    @address = Address.new
    @employee = Employee.find(params[:employee_id])
  end

  def create
    @employee.addresses.create(address_params)

    if @employee.save
      redirect_to employee_addresses_path, notice: "The address has been added!" and return
    end
    flash.now[:danger] = 'Fill out all fields'
    render 'new'
  end

  def edit
    @employee = Employee.find(params[:employee_id])
    @address = @employee.addresses.find(params[:id])
  end

  def update
    @address = @employee.addresses.find(params[:id])
    if @address.update_attributes(address_params)
      redirect_to employee_addresses_path, notice: 'The address has been updated!' and return
    end
    render 'edit'
  end

  def destroy
    @address = @employee.addresses.find(params[:id])
    @address.destroy
    redirect_to employee_addresses_path, notice: 'The address has been deleted!' and return
  end

  private
    def set_employee
      @employee = Employee.find(params[:employee_id])
    end

    def address_params
      params.require(:address).permit(:line, :city, :state, :country, :employee_id)
    end
end
