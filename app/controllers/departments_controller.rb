class DepartmentsController < ApplicationController
	before_action :set_department, only: [:edit, :update, :destroy]

  def index
    @departments = Department.all
  end

  def new
    @department = Department.new
  end

  def create
    @department = Department.new(department_params)

    if @department.save
      redirect_to departments_path, notice: "The department has been created!" and return
    end
    render 'new'
  end

  def edit
  end

  def update
    if @department.update_attributes(department_params)
      redirect_to departments_path, notice: 'The department has been updated!' and return
    end
    render 'edit'
  end

  def destroy
    @department.destroy
    redirect_to departments_path, notice: 'The department has been deleted!' and return
  end

  private
    def set_department
      @department = Department.find(params[:id])
    end

    def department_params
      params.require(:department).permit(:name)
    end
end
