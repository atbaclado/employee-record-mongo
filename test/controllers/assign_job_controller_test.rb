require 'test_helper'

class AssignJobControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get assign_job_index_url
    assert_response :success
  end

  test "should get new" do
    get assign_job_new_url
    assert_response :success
  end

  test "should get create" do
    get assign_job_create_url
    assert_response :success
  end

  test "should get edit" do
    get assign_job_edit_url
    assert_response :success
  end

  test "should get update" do
    get assign_job_update_url
    assert_response :success
  end

  test "should get destroy" do
    get assign_job_destroy_url
    assert_response :success
  end

end
