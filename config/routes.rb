Rails.application.routes.draw do
  root to: 'static_pages#home'

  resources :employees do
    resources :addresses
  end

  resources :users
  resources :departments,      except: [:show]
  resources :job_titles,       except: [:show]
  resources :job_assignments,  except: [:show]

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
